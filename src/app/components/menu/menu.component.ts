import { ChangeDetectorRef, Component, HostListener } from '@angular/core';
import { environment } from '../../../environments/environment';

@Component({
    selector: 'app-menu',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.scss']
})
export class MenuComponent {
    testList = [
        { title: 'Analyzing', artist: 'Dennis Lloyd' },
        { title: 'Am I Wrong', artist: 'Nico & Vinz' },
        { title: 'Because I Can', artist: 'Muck Sticky' },
        { title: 'Bella Ciao', artist: 'Manu Pilas, La casa de papel' },
        { title: 'Catch & Release', artist: 'Matt Simons' }
    ]

    mobileWidth = false
    openSearch = false
    constructor(private cdr: ChangeDetectorRef) {

    }

    /** on window resize */
    @HostListener('window:resize', ['$event'])
    onResize(event) {
        var width = event.target.innerWidth;
        if (width < environment.mobileWidth) {
            this.mobileWidth = true
        } else {
            this.mobileWidth = false
        }
    }

    /** on window click. if clicked outside of search window, close results */
    @HostListener('window:click', ['$event'])
    windowClick(event) {
        var target = event.target || event.srcElement || event.currentTarget;
        console.log(target.id)
        if (target.id !== "searchInput" && target.id !== "searchBtn") {
            this.openSearch = false
        }
    }

    selectedSong(title) {
        console.log("I will show you this title: ", title)
        this.openSearch = false
    }

    sonsListFunc() {
        console.log("open songs list")
    }

    suggestFunc() {
        console.log("open suggestion submission box")
    }

    chordsFunc() {
        console.log("hide/show chords")
    }

    chorusFunc() {
        console.log("hide/show choruses")
    }

    settingsFunc() {
        console.log("hide/show hidden icons")
    }

    clearInput() {
        let x = (<HTMLInputElement>document.getElementById("searchInput"))
        x.value = ""
    }
}