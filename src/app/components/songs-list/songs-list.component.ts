import { Component } from '@angular/core';

@Component({
    selector: 'app-songs-list',
    templateUrl: './songs-list.component.html',
    styleUrls: ['./songs-list.component.scss']
})
export class SongsListComponent {
    
    songsList = [
        'Analyzing',
        'Am I Wrong',
        'Because I Can',
        'Bella Ciao',
        'Catch & Release',
        '\'Cause I Got High',
        'Class Historian',
        'Don\'t Wait',
        'Is It True',
        'It Is Not Meant To Be',
        'Knockin\' On Heaven\'s…',
        'Love Is Alive',
        'Me Gustas Tu',
        'No Cars Go',
        'One More Year',
        'Patience',
        'Perspektiva',
        'Plage',
        'Rebellion',
        'Renegades',
        'Warmth',
        'We Will Rock You',
        'Where\'s My Mind',
        'Young & Unafraid'
    ];

    currentSong = '';

    filteredNames = []; // search results

    selectSong(selectedSong:string){
        this.currentSong = selectedSong
    }

    filterList (titleInput: string) {
        this.filteredNames = [] // search result starts empty
        for (let name of this.songsList) {
            if (name.toUpperCase().includes(titleInput.toUpperCase())) { // does song name include typed string? + eliminating case sensitivity
                this.filteredNames.push(name) // push the song name into the search result
            }
        }
    }

    // searchbox for menu icon
    x = document.getElementById("input")
}