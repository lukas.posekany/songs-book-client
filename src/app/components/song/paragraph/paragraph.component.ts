import { Component } from '@angular/core';

@Component({
    selector: 'app-paragraph',
    templateUrl: './paragraph.component.html',
    styleUrls: ['./paragraph.component.scss']
})
export class ParagraphComponent {
  // in html use ng for to loop through parOrder
    parOrder = ['V1', 'R1', 'V2', 'R2', 'R1', 'V1'];

    // get paragraph text with parID from parOrder
    // parContent[parOrder[0]]
    // ngFor .... smyčka: let par of parOrder" ..... zobrazit: <div>par</div><p>{{parContent[par]}}</p>
    // parContent[par] tam se automaticky dosadi treba parContent['V1']
    parContent = { // muze se to jmenovat parText
      V1: 'text V1.......... tohle bude text parametru',
      R1: 'refren2...........',
      V2: 'version 2...........',
      //........
    }


}
