import { Component } from '@angular/core';
import { ParagraphComponent } from './paragraph/paragraph.component'

@Component({
    selector: 'app-song',
    templateUrl: './song.component.html',
    styleUrls: ['./song.component.scss']
})
export class SongComponent {
    song = {
            id: 1,
            name: 'Bella Ciao',
            artist: [{
                    id: 1, name: 'Manu Pilas', }, {
                    id: 2, name: '',
                    }],
            language: 'CZ',
            paragraphsOrder: ['V1', 'V2', 'V3', 'V4', 'V5', 'V6', 'OU'],
            paragraphsContent: [
                {
                label: "V1",
                content: 'Una mattina mi sono alzato\nO bella ciao, bella ciao, bella ciao, ciao, ciao\nUna mattina mi sono alzato\nE ho trovato l\'invasor'
            }, {
                label: "V2",
                content: 'O partigiano portami via\nO bella ciao, bella ciao, bella ciao, ciao, ciao\nO partigiano portami via\nChe mi sento di morir'
            }, {
                label: "V3",
                content: 'E se io muoio da partigiano\nO bella ciao, bella ciao, bella ciao, ciao, ciao\nE se io muoio da partigiano\nTu mi devi seppellir'
            }, {
                label: "V4",
                content: 'E seppellire lassù in montagna\nO bella ciao, bella ciao, bella ciao, ciao, ciao\nE seppellire lassù in montagna\nSotto l\'ombra di un bel fior'
            }, {
                label: "V5",
                content: 'Tutte le genti che passeranno\nO bella ciao, bella ciao, bella ciao, ciao, ciao\nTutte le genti che passeranno\nMi diranno: "Che bel fior"'
            }, {
                label: "V6",
                content: 'Questo è il fiore del partigiano\nO bella ciao, bella ciao, bella ciao, ciao, ciao\nQuesto è il fiore del partigiano\nMorto per la libertà'
            }, {
                label: "OU",
                content: 'E questo è il fiore del partigiano\nMorto per la libertà'
            }]
        }
    getContent(parLabel) {
        return this.song.paragraphsContent.find((par:any) => {
            return par.label === parLabel
        })
    }
    
}