import { ChangeDetectorRef, Component, HostListener } from '@angular/core';
import { environment } from '../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'songs-book-client';
  mobileWidth = false
  constructor(private cdr: ChangeDetectorRef) {

  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    var width = event.target.innerWidth;
    if (width < environment.mobileWidth) {
      this.mobileWidth = true
    } else {
      this.mobileWidth = false
    }
  }
}
