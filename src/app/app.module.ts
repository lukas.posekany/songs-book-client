import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { MenuComponent } from '../app/components/menu/menu.component';
import { PlaylistListComponent } from '../app/components/playlist-list/playlist-list.component';
import { SongsListComponent } from '../app/components/songs-list/songs-list.component';
import { ButtonComponent } from './components/button/button.component';
import { InfoComponent } from './components/info/info.component';
import { SongComponent } from './components/song/song.component';
import { ParagraphComponent } from './components/song/paragraph/paragraph.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
  declarations: [
    AppComponent,
    ButtonComponent,
    InfoComponent,
    MenuComponent,
    PlaylistListComponent,
    SongComponent,
    ParagraphComponent,
    SongsListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatIconModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
