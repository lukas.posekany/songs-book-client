# SongsBookClient

## NOTES
* [html](./NOTES/html.md)
* [css](./NOTES/css.md)
* [scss](./NOTES/SCSS.md)
* [JS](./NOTES/JavaScript.md)
* [TS](./NOTES/TypeScript.md)
* [Angular](./NOTES/Angular.md)

## Git
organize file structure, handle branches and versions
Commands: 
* `git pull origin master`
* `git checkout branchName`
* `git push branchName`
* `git merge origin master`

in VS code:
* bottom bar from left
    * name of branch
    * push + pull changes of branch
    * Git Graph

* Git Graph:
    * to merge changes from one branch to another: right click on branch to be merged + "merge into current branch" (something on master changed and you want that changes in your branch)

* Source Control (left menu)
    * add changes by clicking +
    * wright commit message
    * ... - Push 

## Development server
in Power Shell navigate to root of project (`ls` - list current folder, `cd` - change directory, ../ for one level back)

* Run `ng serve` for a dev server.
* Navigate to `http://localhost:4200/`.
* The app will automatically reload if you change any of the source files.

## File Structure
* /src/
    * index.html - root html file, just for header, not content
    * app/
        * app.component.html - root component of project
        * components/ - other components of project

## Project Components
* menu
* playlist-list
* songs-list
* info
* song
    * paragraph
* button
    - `<app-button iconName="accessible_forward"></app-button>`
    - icons in: https://material.io/resources/icons/?style=baseline
